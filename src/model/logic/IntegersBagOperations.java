
package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}

		}
		return min;
	}
	
//	 public double getStandardDev (IntegersBag bag ) {
//		    double prom, sum = 0; 
//		    int value=0, n=0;
//		    prom = computeMean(bag);
//
//		    if(bag != null){
//				Iterator<Integer> iter = bag.getIterator();
//				while(iter.hasNext()){
//					
//					value = iter.next();
//					sum += Math.pow ( value - prom, 2 );
//					n++;
//				}
//
//			}
//		    return Math.sqrt ( sum / ( double ) n );
//		  }
	
	public int getMultiplication (IntegersBag bag){
		int mult = 1;
		
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			
			while(iter.hasNext()){
				
				mult = iter.next()*mult;
				
			}
			
		}
		return mult;
	}
	 
	 public int getSum (IntegersBag bag){
			int sum = 0;
			
			if(bag != null){
				Iterator<Integer> iter = bag.getIterator();
				
				while(iter.hasNext()){
					
					sum += iter.next();
					
				}
				
			}
			return sum;
		}
}
